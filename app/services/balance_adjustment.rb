# frozen_string_literal: true

class BalanceAdjustment
  def self.call(user, amount, currency)
    transfer = Transfer.create!(
      user: user,
      amount: amount,
      currency: currency
    )
    new(transfer).execute
  end

  attr_accessor :transfer

  def initialize(transfer)
    @transfer = transfer
  end

  delegate :transition_to!, :user, :amount, :currency, to: :transfer
  delegate :currency, to: :user, prefix: true

  def execute
    Transfer.transaction do
      transition_to! :completed
      transfer_money
    end
  end

  private

  def transfer_money
    CasinoModule::Players.transfer(
      player_login: user.login,
      currency: user_currency,
      amount: user_amount
    )
  end

  def user_amount
    if user_currency == currency
      amount
    else
      Currency::Amount.new(currency: currency, amount: amount).in(user.currency)
    end
  end
end
